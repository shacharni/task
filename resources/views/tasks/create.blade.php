@extends('layouts.app')

@section('content')
<h1>Create a new task</h1>
<form method='post' action="{{action('TaskController@store')}}">
    {{csrf_field()}}

    <div class="form-group">
    <h2>Add a task</h2>
        <label for ="title">title </label>
        <input type="text" class= "form-control" name="title">
        </div>
    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection